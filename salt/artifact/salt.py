"""
    artifact module to manage the download of salt artifacts
"""
import aiohttp
import os
from distutils.version import LooseVersion


async def fetch(hub, session, url, download=False, location=False):
    """
    Fetch a url and return json. If downloading artifact
    return the download location.
    """
    async with session.get(url) as resp:
        if resp.status == 200:
            if download:
                with open(location, "wb") as fn_:
                    fn_.write(await resp.read())
                return location
            return await resp.json()
        hub.log.critical(f"Cannot query url {url}. Returncode {resp.status} returned")
        return False


async def version(hub, t_os):
    """
    Query latest version from repo if user does not define version
    """
    ver = hub.OPT["heist"].get("artifact_version")
    if ver:
        return ver

    url = "https://repo.saltstack.com/salt-bin/repo.json"
    async with aiohttp.ClientSession() as session:
        data = await hub.artifact.salt.fetch(session, url)
        if not data:
            hub.log.critical(
                f"Query to {url} failed, falling back to" f"pre-downloaded artifacts"
            )
            return False
        # we did not set version so query latest version from the repo
        for binary in data[t_os]:
            b_ver = data[t_os][binary]["version"]
            if not ver:
                ver = b_ver
                continue
            if LooseVersion(b_ver) > LooseVersion(ver):
                ver = b_ver
        return ver


async def get(
    hub, t_name: str, t_type: str, art_dir: str, t_os: str, ver: str = ""
) -> str:
    """
    Download artifact if does not already exist. If artifact
    version is not specified, download the latest from pypi
    """
    if ver:
        art_n = f"salt-{ver}"
    else:
        art_n = "salt"
    url = f"https://repo.saltstack.com/salt-bin/{t_os}/{art_n}"

    # TODO this needs to work for windows too
    # Ensure that artifact directory exists
    ret = await getattr(hub, f"tunnel.{t_type}.cmd")(t_name, f"mkdir -p {art_dir}")
    assert not ret.returncode, ret.stderr
    location = os.path.join(art_dir, art_n)

    # check to see if artifact already exists
    if hub.heist.salt.minion.latest("salt", art_dir, version=ver):
        hub.log.info(f"The Salt artifact {ver} already exists")
        return location

    # download artifact
    async with aiohttp.ClientSession() as session:
        hub.log.info(f"Downloading the artifact {art_n} to {art_dir}")
        await hub.artifact.salt.fetch(session, url, download=True, location=location)

    # ensure artifact was downloaded
    if not os.path.isdir(art_dir):
        hub.log.critical(f"The target directory '{art_dir}' does not exist")
        return ""
    elif not any(ver in x for x in os.listdir(art_dir)):
        hub.log.critical(
            f"Did not find the {ver} artifact in {art_dir}."
            f" Untarring the artifact failed or did not include version"
        )
        return ""
    else:
        return location


async def deploy(hub, t_name: str, t_type: str, art_dir: str, t_os: str, ver: str):
    ...


async def clean(hub, t_name: str, t_type: str, art_dir: str, t_os: str, ver: str):
    ...
