"""
    artifact module to manage the download of grains artifacts
"""
import aiohttp
import hashlib
import os

__virtualname__ = "grains"


def checksum(hub, local_file_path: str) -> str:
    md5 = hashlib.md5()

    if os.path.exists(local_file_path):
        with open(local_file_path, "rb") as fd:
            for line in fd:
                md5.update(line)

    return md5.hexdigest()


async def version(hub, t_os):
    ...


async def get(hub, t_name: str, t_type: str, art_dir: str, t_os: str, ver: str = None):
    """
    Fetch a url return the download location.
    """
    os.makedirs(art_dir, exist_ok=True)

    artifact_url = f"http://artifactory.saltstack.net/artifactory/open-production/grains/{t_os}/{ver}/grains"

    async with aiohttp.ClientSession() as session:
        async with session.get(artifact_url) as response:
            local_grains_bin = os.path.join(
                art_dir, f"{response.url.name}-{ver}-{t_os}"
            )
            hub.log.info(f"binary to: {local_grains_bin}")
            if hub.artifact.grains.checksum(local_grains_bin) == response.headers.get(
                "X-Checksum-Md5"
            ):
                return local_grains_bin

            with open(local_grains_bin, "wb") as fd:
                async for data in response.content.iter_chunked(1024):
                    fd.write(data)
                os.chmod(local_grains_bin, 0o644)
    return local_grains_bin


async def deploy(hub, t_name: str, t_type: str, art_dir: str, t_os: str, ver: str):
    ...


async def clean(hub, t_name: str, t_type: str, art_dir: str, t_os: str, ver: str):
    ...
