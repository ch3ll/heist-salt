import tempfile
from typing import Dict

CONFIG = """master: {master}
master_port: {master_port}
publish_port: {publish_port}
root_dir: {root_dir}
"""


async def run(hub, remotes: Dict[str, Dict[str, str]]):
    ...


def mk_config(hub, root_dir: str, t_name):
    """
    Create a minion config to use with this execution and return the file path
    for said config
    """
    _, path = tempfile.mkstemp()
    roster = hub.heist.ROSTERS[t_name]
    master = roster.get("master", "127.0.0.1")

    with open(path, "w+") as wfp:
        wfp.write(
            CONFIG.format(
                master=master,
                master_port=roster.get("master_port", 44506),
                publish_port=roster.get("publish_port", 44505),
                root_dir=root_dir,
            )
        )
    return path


def bootstrap(hub):
    # Set up a persistent minion on the target
    # Calls heist.bin.deploy()
    ...


def teardown(hub):
    # Tear down a persistent minion
    ...
